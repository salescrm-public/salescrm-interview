
## React Test Task

Build a simple React-Redux application which displays images from cat API. Here's the API [https://docs.thecatapi.com/api-reference/images/images-search](https://docs.thecatapi.com/api-reference/images/images-search)

Example URL to fetch 10 cats with category hats:

[https://api.thecatapi.com/v1/images/search?limit=10&category_ids=1](https://api.thecatapi.com/v1/images/search?limit=10&category_ids=1)

Example URL to fetch the categories: [https://api.thecatapi.com/v1/categories](https://api.thecatapi.com/v1/categories)

The page is a simple app that loads a sidebar of all categories and displays 10 cat images in the main display.

- All the categories are clickable - you can click on them and choose a different category 
- There is a button to load more cat images at the bottom, which will load 10 more cat images
- The app must be built with react-redux 
- The logic of react-redux should be unit-tested 
- Don't use any premade UI kit lib, write all styles purely, (jss library is highly recommended) – global styles not acceptable.
- Aesthetics and clean code are important when building this small prototype. 

Here's what we will be evaluating when we will review the solution:

- Follows the requirements - app does what is required 
- Clean code - there is no dead/commented out code, code is easy to read and understand, well structured 
- JS - code is tested, state handled in redux, follows common conventions 
- UI - looks nice and is responsive 
- You’ll get extra points by using React router with lazy loading and Typescript. 

Upload the code to git and send the link to us. Also, please attach a short video/gif of the application in action.

You have 3 days  to complete the test, but it should take only a few hours for an experienced dev.
